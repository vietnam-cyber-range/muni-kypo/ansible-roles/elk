# Ansible role - KYPO elk

This role deploys Elasticsearch, Logstash and optionally Kibana behind Nginx HTTPS reverse proxy for KYPO.

[[_TOC_]]

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

* At least 1GB RAM on target machine.

## Role parameters

Optional.

* `elk_logstash_pipelines` - Content of logstash [pipelines.yml](https://www.elastic.co/guide/en/logstash/current/multiple-pipelines.html) configuration file where value of `path.config` is path on the Ansible control node (default: `[]`).
* `elk_kibana_enabled` - If Kibana should be deployed (default: `False`).
* `elk_kibana_username` - Username of Kibana user (default: `kibana`).
* `elk_kibana_password` - Password of Kibana user (default: `change_me`).
* `elk_kibana_cert` - Content of HTTPS certificate. If undefined, certificate and private key will be generated (default: `None`).
* `elk_kibana_cert_key` - Content of HTTPS private key. If undefined, certificate and private key will be generated (default: `None`).
* `elk_docker_network_name` - The name of the Docker network used by syslog-ng container. If not specified, default Docker network will be used (default: `''`).

## Example

The simplest example of the full ELK stack installation with one pipeline.

```yml
roles:
    - role: elk
      elk_logstash_pipelines:
          - path.config: /path/to/pipeline.conf
            pipeline.id: '1'
            pipeline.workers: 2
      elk_kibana_enabled: True
      become: yes
```
